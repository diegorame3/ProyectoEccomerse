package com.develop.DAO;



import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputFilter.Config;
import java.sql.Connection;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.develop.config.Conexion;
import com.develop.interfaces.IUsuario;
import com.develop.models.MArticulo;
import com.develop.models.MCategoria;
import com.develop.models.MUsuario;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import java.sql.PreparedStatement;


public class UsuarioDAO implements IUsuario{
	
	
	Conexion conecctions = new Conexion();
	Connection consql;
	PreparedStatement ps;
	ResultSet rs;
	
	ArrayList<MUsuario> lista = new ArrayList<MUsuario>();
	MUsuario usuario;

	
	public boolean verificarConexion() {
		boolean res =(conecctions.getConnection()!=null)?true:false;
		return res;
		
	}
	
	
	@Override
	public String validaUsuario(String usuario,String password) {
		
		String sql = "SELECT * FROM `usuario` WHERE nombreUsuario='"+usuario+"'  AND  password ='"+password+"'";
		String res="11";
		
		if (verificarConexion()) {
			//continuar con la ejecucion
			try {
				consql = conecctions.getConnection();
				ps=  consql.prepareStatement(sql);
				rs=ps.executeQuery();
				if (rs.next()) {
					System.out.println(rs.getString("tipoUsuario"));
					res="00"+","+rs.getString("tipoUsuario");	//retorna el tipo de usuario para inicar sesion
				}
			} catch (Exception e) {
				System.out.println("El error es: "+e);
			}
		}else {
			System.out.println("\n-----------conexion mala-----------\n");
			//redireccionar a login con error
			
			
		}
		
		
		
	
		
		//sinonimo de OK
		return res;
	}

	@Override
	public String crearUsuario(MUsuario usuario) {
		// TODO Auto-generated method stub
		try {
			  String sql = "INSERT INTO `usuario`(`nombreUsuario`,`nombre`,`password`,`telefono`,`email`,`tipoUsuario`) VALUES ('"+usuario.getNombreUsuario()+"','"+usuario.getNombre()+"','"+usuario.getPassword()+"','"+usuario.getTelefono()+"','"+usuario.getCorreo()+"','"+usuario.getTipoUsuario()+"')";
		      ps = consql.prepareStatement(sql);
		      ps.executeUpdate();
		      System.out.println("ADD correcto");
		} catch (Exception e) {
			System.out.println("El error es: "+e);
		}
		return null;
	}

	@Override
	public List<MUsuario> getUsuario() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		
		String sql = "SELECT * FROM `usuario`";
		try {
			consql = conecctions.getConnection();
			ps=  consql.prepareStatement(sql);
			rs=ps.executeQuery();
			while (rs.next()) {
				usuario = new MUsuario();
                usuario.setNombre(rs.getString("nombre"));
                usuario.setNombreUsuario(rs.getString("nombreUsuario"));
                usuario.setTelefono(rs.getString("telefono"));
                usuario.setCorreo(rs.getString("email"));
                usuario.setId(rs.getInt("idUsuario"));
                usuario.setFecha(LocalDateTime.parse(rs.getString("fechaCreacion").substring(0, 19), formatter));
                usuario.setPassword(rs.getString("password"));
                usuario.setTipoUsuario(rs.getInt("tipoUsuario"));
                lista.add(usuario);
            }
            rs.close();
            ps.close();
		} catch (Exception e) {
			System.out.println("El error es: "+e);
		}
		return lista;
	}

	@Override
	public String eliminarUsuario(int id) {
		
		String sql = "DELETE FROM `usuario` WHERE `usuario`.`idUsuario` = "+id;
		try {
			consql = conecctions.getConnection();
			ps=  consql.prepareStatement(sql);
			ps.executeUpdate();
            ps.close();
            System.out.println("Eliminado con exito");
            
		} catch (Exception e) {
			System.out.println("El error al eliminar  es: "+e);
		}
		return null;
	}

	
	public String editarUsuario(MUsuario usuario) {
		String respuesta="00";
		
		try {
			System.out.println("Entra a editarMetodo "+usuario.getPassword());
			String sql = "UPDATE `usuario` SET nombreUsuario=?, nombre=?, password=? , telefono=?, email=? WHERE idUsuario=?";
			consql = conecctions.getConnection();
			ps = consql.prepareStatement(sql);
			ps.setString(1, usuario.getNombreUsuario());
			ps.setString(2, usuario.getNombre());
			ps.setString(3, usuario.getPassword());
			ps.setString(4, usuario.getTelefono());
			ps.setString(5, usuario.getCorreo());
			ps.setInt(6, usuario.getId());
			int res = ps.executeUpdate();
			if (res> 0) {
			    System.out.println("Actualizacion exitosa");
			}
		} catch (Exception e) {
			System.out.println("Error en la actualiazacion: "+e);
			respuesta="11";
		}
		return respuesta;
	}
	
	
	public String imprimir() {
		System.out.println("entra a dao imprimir");
		File archivo = new File("C:\\Users\\ogeid\\Documents\\workspace-spring-tool-suite-4-4.12.0.RELEASE\\Proyecto_Eccomers\\src\\com\\develop\\DAO/usuarios.txt");
		try {
			boolean bandera= archivo.createNewFile();
			FileWriter fileWriter =new FileWriter(archivo);
			System.out.println("bandera:"+bandera);
			for (MUsuario mUsuario : getUsuario()) {
				//escribir cada 	articulo en el archivo
				System.out.println(mUsuario.getNombre());
				fileWriter.write(mUsuario.getNombre()+"   Tipo:"+mUsuario.getTipoUsuario()+"\n");
			}
			fileWriter.close();
			
		} catch (IOException e) {
			System.out.println("Error en la apertura del archivo");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return"";
	}
	
	public String imprimirPDF()  {
		Document pdf = new Document();
		
		// Se crea el OutputStream para el fichero donde queremos dejar el pdf.
		FileOutputStream ficheroPdf;
		try {
			ficheroPdf = new FileOutputStream("C:\\Users\\ogeid\\Documents\\workspace-spring-tool-suite-4-4.12.0.RELEASE\\Proyecto_Eccomers\\src\\com\\develop\\DAO/ListaUsuarios.pdf");
			
			// Se asocia el documento al OutputStream y se indica que el espaciado entre
			// lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
			PdfWriter.getInstance(pdf,ficheroPdf).setInitialLeading(20);
			//Se abre el documento.
			pdf.open();
			
			//se esccribe en una tabla
			pdf.add(new Paragraph("Lista de Articulos"));
			//
			for (MUsuario mUsuario : getUsuario()) {
				//escribir cada 	articulo en el archivo
				pdf.add(new Paragraph(mUsuario.getNombre()));
			}
			
			pdf.close();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			System.out.println("El error de pdf"+e1);
		}
		return "";
	}
	
	public String imprimirEXCEL()  {
		int row=1;
		
		try { 
			  File xlsFile = new File("C:\\Users\\ogeid\\Documents\\workspace-spring-tool-suite-4-4.12.0.RELEASE\\Proyecto_Eccomers\\src\\com\\develop\\DAO/ListaUsuarios.xls");
		      // Crea un libro de trabajo
		      WritableWorkbook workbook = Workbook.createWorkbook(xlsFile);
		      // Crea una hoja de trabajo
		      WritableSheet sheet = workbook.createSheet("Usuarios", 0);
		      sheet.addCell(new Label(0,0, "Usuarios"));
		      for (MUsuario mUsuario : getUsuario()) {
		    	  sheet.addCell(new Label(0, row, mUsuario.getNombre()));
		    	  row++;
				
			}
		      workbook.write();
		      workbook.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
		return "";
	}
	
	
}

	
	

