package com.develop.DAO;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.sql.Date;

import com.develop.config.Conexion;
import com.develop.interfaces.IArticulo;
import com.develop.models.MArticulo;
import com.develop.models.MCategoria;
import com.develop.models.MUsuario;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class ArticuloDAO implements IArticulo{

	Conexion conecctions = new Conexion();
	Connection consql;
	PreparedStatement ps;
	ResultSet rs;
	ArrayList<MArticulo> lista = new ArrayList<MArticulo>();
	MArticulo articulo;
	
	
	
	
	
	@Override
	public ArrayList<MArticulo> getArticulo() {
		String sql = "SELECT * FROM `producto`";

		try {
			consql = conecctions.getConnection();
			ps=  consql.prepareStatement(sql);
			rs=ps.executeQuery();
			while (rs.next()) {
				articulo = new MArticulo();
                articulo.setNombre(rs.getString("nombre"));
                articulo.setPrecio(Integer.parseInt(rs.getString("precio")));
                articulo.setStock(Integer.parseInt(rs.getString("stock")));
                articulo.setDescripcion(rs.getString("descripcion"));
                articulo.setDescuento(rs.getInt("descuento"));
                if (rs.getString("fechaExDescuento")!=null) {
                	articulo.setFechaExDescuento(LocalDate.parse(rs.getString("fechaExDescuento")));
				}else {
					articulo.setFechaExDescuento(null);
				}
                articulo.setIdArticulo(rs.getInt("idProducto"));
                articulo.setIdCategoria(rs.getInt("idCategoria"));
                articulo.setFechaCreacion(LocalDateTime.parse(rs.getString("fechaCreacion").replace(" ", "T")));
                lista.add(articulo);
            }
            rs.close();
            ps.close();
		} catch (Exception e) {
			System.out.println("El error en ArticuloDAO es: "+e);
		}
		return lista;
	}

	@Override
	public String addArticulo(MArticulo articulo) {
		String respuesta="00";
		String sql;
		try {
			if (articulo.getFechaExDescuento()!=null) {
				 sql = "INSERT INTO `producto`(`nombre`,`precio`,`descripcion`,`stock`,`idCategoria`,`descuento`,`fechaExDescuento`) VALUES ('"+articulo.getNombre()+"','"+articulo.getPrecio()+"','"+articulo.getDescripcion()+"','"+articulo.getStock()+"','"+articulo.getIdCategoria()+"','"+articulo.getDescuento()+"','"+articulo.getFechaExDescuento()+"')";
			}else{
				 sql = "INSERT INTO `producto`(`nombre`,`precio`,`descripcion`,`stock`,`idCategoria`,`descuento`) VALUES ('"+articulo.getNombre()+"','"+articulo.getPrecio()+"','"+articulo.getDescripcion()+"','"+articulo.getStock()+"','"+articulo.getIdCategoria()+"','"+articulo.getDescuento()+"')";
			}
			 
			  consql=conecctions.getConnection();
		      ps = consql.prepareStatement(sql);
		      ps.executeUpdate();
		      System.out.println("ADD articulo correcto");
		} catch (Exception e) {
			System.out.println("El error es: "+e);
			respuesta="11";
		}
		return respuesta;
	}

	@Override
	public String eliminaArticulo(int id) {
		String sql = "DELETE FROM `producto` WHERE `producto`.`idProducto` = "+id;
		try {
			consql = conecctions.getConnection();
			ps=  consql.prepareStatement(sql);
			ps.executeUpdate();
            ps.close();
            System.out.println("Eliminado con exito");
            
		} catch (Exception e) {
			System.out.println("El error al eliminar  es: "+e);
		}
		return null;
	}

	
	public String editarArticulo(MArticulo ariculo) {
		String respuesta="00";
		
		try {
			//System.out.println("Entra a editarMetodo "+usuario.getPassword());
			String sql = "UPDATE `producto` SET nombre=?, precio=?, descripcion=? , stock=?, descuento=?, fechaExDescuento=? WHERE idProducto=?";
			consql = conecctions.getConnection();
			ps = consql.prepareStatement(sql);
			ps.setString(1, ariculo.getNombre());
			ps.setInt(2, (int) ariculo.getPrecio());
			ps.setString(3, ariculo.getDescripcion());
			ps.setInt(4, (int)ariculo.getStock());
			ps.setInt(5, ariculo.getDescuento());
			ps.setDate(6,Date.valueOf(ariculo.getFechaExDescuento()));
			ps.setInt(7, ariculo.getIdArticulo());
			int res = ps.executeUpdate();
			if (res> 0) {
			    System.out.println("Actualizacion exitosa");
			}
		} catch (Exception e) {
			System.out.println("Error en la actualiazacion: "+e);
			respuesta="11";
		}
		return respuesta;
	}

	
	public String imprimir() {
		System.out.println("entra a dao imprimir");
		File archivo = new File("C:\\Users\\ogeid\\Documents\\workspace-spring-tool-suite-4-4.12.0.RELEASE\\Proyecto_Eccomers\\src\\com\\develop\\DAO/articulos.txt");
		try {
			boolean bandera= archivo.createNewFile();
			FileWriter fileWriter =new FileWriter(archivo);
			System.out.println("bandera:"+bandera);
			for (MArticulo mArticulo : getArticulo()) {
				//escribir cada 	articulo en el archivo
				System.out.println(mArticulo.getNombre());
				fileWriter.write(mArticulo.getNombre()+"\n");
			}
			fileWriter.close();
			
		} catch (IOException e) {
			System.out.println("Error en la apertura del archivo");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return"";
	}
	
	public String imprimirPDF()  {
		Document pdf = new Document();
		
		// Se crea el OutputStream para el fichero donde queremos dejar el pdf.
		FileOutputStream ficheroPdf;
		try {
			ficheroPdf = new FileOutputStream("C:\\Users\\ogeid\\Documents\\workspace-spring-tool-suite-4-4.12.0.RELEASE\\Proyecto_Eccomers\\src\\com\\develop\\DAO/ListaArticulos.pdf");
			
			// Se asocia el documento al OutputStream y se indica que el espaciado entre
			// lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
			PdfWriter.getInstance(pdf,ficheroPdf).setInitialLeading(20);
			//Se abre el documento.
			pdf.open();
			
			//se esccribe en una tabla
			pdf.add(new Paragraph("Lista de Articulos"));
			//
			for (MArticulo mArticulo : getArticulo()) {
				//escribir cada 	articulo en el archivo
				pdf.add(new Paragraph(mArticulo.getNombre()));
			}
			
			pdf.close();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			System.out.println("El error de pdf"+e1);
		}
		return "";
	}
	
	public String imprimirEXCEL()  {
		int row=1;
		
		try { 
			  File xlsFile = new File("C:\\Users\\ogeid\\Documents\\workspace-spring-tool-suite-4-4.12.0.RELEASE\\Proyecto_Eccomers\\src\\com\\develop\\DAO/ListaArticulos.xls");
		      // Crea un libro de trabajo
		      WritableWorkbook workbook = Workbook.createWorkbook(xlsFile);
		      // Crea una hoja de trabajo
		      WritableSheet sheet = workbook.createSheet("Articulos", 0);
		      sheet.addCell(new Label(0,0, "Usuarios"));
		      for (MArticulo mArticulo : getArticulo()) {
		    	  sheet.addCell(new Label(0, row, mArticulo.getNombre()));
		    	  row++;
				
			}
		      workbook.write();
		      workbook.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
		return "";
	}

	@Override
	public ArrayList<MArticulo> getArticuloCategoria(int idCategoria) {
		String sql = "SELECT * FROM `producto` WHERE `idCategoria`="+idCategoria;

		try {
			consql = conecctions.getConnection();
			ps=  consql.prepareStatement(sql);
			rs=ps.executeQuery();
			while (rs.next()) {
				articulo = new MArticulo();
                articulo.setNombre(rs.getString("nombre"));
                articulo.setPrecio(Integer.parseInt(rs.getString("precio")));
                articulo.setStock(Integer.parseInt(rs.getString("stock")));
                articulo.setDescripcion(rs.getString("descripcion"));
                articulo.setDescuento(rs.getInt("descuento"));
                if (rs.getString("fechaExDescuento")!=null) {
                	articulo.setFechaExDescuento(LocalDate.parse(rs.getString("fechaExDescuento")));
				}else {
					articulo.setFechaExDescuento(null);
				}
                articulo.setIdArticulo(rs.getInt("idProducto"));
                articulo.setIdCategoria(rs.getInt("idCategoria"));
                articulo.setFechaCreacion(LocalDateTime.parse(rs.getString("fechaCreacion").replace(" ", "T")));
                lista.add(articulo);
            }
            rs.close();
            ps.close();
		} catch (Exception e) {
			System.out.println("El error en ArticuloDAO es: "+e);
		}
		return lista;
	}
	
}
