
package com.develop.models;


public class MDireccion {
    private int idDireccion;
    private String calle;
    private String colonia;
    private String estado;
    private String municipio;
    private String cp;
    private String numExterior;
    private String numInterior;

    public MDireccion() {
    }
    
    public MDireccion(int idDireccion, String calle, String colonia, String estado, String municipio, String cp, String numExterior, String numInterior) {
        this.idDireccion = idDireccion;
        this.calle = calle;
        this.colonia = colonia;
        this.estado = estado;
        this.municipio = municipio;
        this.cp = cp;
        this.numExterior = numExterior;
        this.numInterior = numInterior;
    }

    public int getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(int idDireccion) {
        this.idDireccion = idDireccion;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getNumExterior() {
        return numExterior;
    }

    public void setNumExterior(String numExterior) {
        this.numExterior = numExterior;
    }

    public String getNumInterior() {
        return numInterior;
    }

    public void setNumInterior(String numInterior) {
        this.numInterior = numInterior;
    }
    
    
    
}
