
package com.develop.models;

import java.time.LocalDate;


public class MCarrito {
    
    private int idCarrito;
    private int total;
    private LocalDate fecha;

    public MCarrito() {
    }

    public MCarrito(int idCarrito, int total, LocalDate fecha) {
        this.idCarrito = idCarrito;
        this.total = total;
        this.fecha = fecha;
    }

    public int getIdCarrito() {
        return idCarrito;
    }

    public void setIdCarrito(int idCarrito) {
        this.idCarrito = idCarrito;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }
    
    
    
    
    
}
