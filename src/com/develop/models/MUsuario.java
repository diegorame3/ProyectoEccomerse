
package com.develop.models;

import java.time.LocalDateTime;
import java.util.Date;

public class MUsuario {
    
    private String nombre;
    private String nombreUsuario;
    private String password;
    private int id;
    private String telefono;
    private String correo;
    private LocalDateTime fecha;
    private int tipoUsuario;

    public MUsuario() {
    }

	public MUsuario(String nombre, String nombreUsuario, String password, int id, String telefono, String correo,
			LocalDateTime fecha, int tipoUsuario) {
		super();
		this.nombre = nombre;
		this.nombreUsuario = nombreUsuario;
		this.password = password;
		this.id = id;
		this.telefono = telefono;
		this.correo = correo;
		this.fecha = fecha;
		this.tipoUsuario = tipoUsuario;
	}






	public int getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(int tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    
    
    
}
