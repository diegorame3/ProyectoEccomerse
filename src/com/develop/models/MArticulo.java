
package com.develop.models;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MArticulo {
    private int idArticulo;
    private int idCategoria;
    private String nombre;
    private int stock;
    private float precio;
    private String descripcion;
    private int descuento;
    private LocalDateTime fechaCreacion;
    private LocalDate fechaExDescuento;

    public MArticulo() {
    }
    
    

    public MArticulo(int idArticulo, String nombre, int contenido, float precio, String descripcion) {
		super();
		this.idArticulo = idArticulo;
		this.nombre = nombre;
		this.stock = contenido;
		this.precio = precio;
		this.descripcion = descripcion;
	}


    

	public int getDescuento() {
		return descuento;
	}



	public void setDescuento(int descuento) {
		this.descuento = descuento;
	}



	public LocalDateTime getFechaCreacion() {
		return fechaCreacion;
	}



	public void setFechaCreacion(LocalDateTime fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}



	public LocalDate getFechaExDescuento() {
		if(fechaExDescuento==null) {
			return LocalDate.parse("0001-01-01", DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		}else {
			return fechaExDescuento;
		}
		
		
	}



	public void setFechaExDescuento(LocalDate fechaExDescuento) {
		this.fechaExDescuento = fechaExDescuento;
	}



	public int getIdCategoria() {
		return idCategoria;
	}



	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}



	public String getDescripcion() {
		return descripcion;
	}



	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	public int getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(int idArticulo) {
        this.idArticulo = idArticulo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int contenido) {
        this.stock = contenido;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    
    
}
