
package com.develop.models;

import java.time.LocalDate;


public class MResena {
    
    private int idResena;
    private String titulo;
    private String contenido;
    private int puntuacion;
    private LocalDate fecha;

    public MResena() {
    }

    public MResena(int idResena, String titulo, String contenido, int puntuacion, LocalDate fecha) {
        this.idResena = idResena;
        this.titulo = titulo;
        this.contenido = contenido;
        this.puntuacion = puntuacion;
        this.fecha = fecha;
    }

    public int getIdResena() {
        return idResena;
    }

    public void setIdResena(int idResena) {
        this.idResena = idResena;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }
    
    
    
            
    
}
