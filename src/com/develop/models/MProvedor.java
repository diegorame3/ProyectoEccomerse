
package com.develop.models;


public class MProvedor {
    
    private int idProvedor;
    private String nombre;
    private String rfc;

    public MProvedor() {
    }

    public MProvedor(int idProvedor, String nombre, String rfc) {
        this.idProvedor = idProvedor;
        this.nombre = nombre;
        this.rfc = rfc;
    }

    public int getIdProvedor() {
        return idProvedor;
    }

    public void setIdProvedor(int idProvedor) {
        this.idProvedor = idProvedor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }
    
    
    
}
