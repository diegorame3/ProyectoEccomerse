package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.UsuarioDAO;

import com.develop.models.MUsuario;

/**
 * Servlet implementation class UsuarioC
 */
@WebServlet("/UsuarioC")
public class UsuarioC extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UsuarioC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: Usuario").append(request.getContextPath());
		String usuario;
		String password;
		
		usuario=request.getParameter("usuario");
		password=request.getParameter("contrasena");
		//System.out.println("Parametros");
		//System.out.println(usuario+password);
		
		UsuarioDAO dao = new UsuarioDAO();
		MUsuario per = null;
		
//iniciaSesion
		if (request.getParameter("iniciarSecion")!=null) {
			String res=dao.validaUsuario(usuario, password);
			//System.out.println(res);
			
			if (res.split(",")[0].equals("00")) {
				System.out.println("Redireccionando ");
				//definiendo la pagina a mostrar     0:Usuario   1:Admin
				if (res.split(",")[1].equals("1")) {
					response.sendRedirect("/Proyecto_Eccomers/vistas/admin/adminPage/admin.jsp");
				}else {
					response.sendRedirect("/Proyecto_Eccomers/vistas/main/main.jsp");
				}
				
			}else {
				System.out.println("No valido");
				response.sendRedirect("/Proyecto_Eccomers/vistas/login/login.jsp");
			}
			
			
		}
		
//borrar
		if (request.getParameter("borrar")!=null) {
			System.out.println("entra a borrado id: "+request.getParameter("id"));
			dao.eliminarUsuario(Integer.parseInt(request.getParameter("id")));
			
			//Se redirecciona a pesta�a usuario o administrador
			if (request.getParameter("tipoUsuario").equals("0")) {
				response.sendRedirect("/Proyecto_Eccomers/vistas/admin/adminUsuario/adminUsuario.jsp");
			}else {
				response.sendRedirect("/Proyecto_Eccomers/vistas/admin/adminAdministradores/adminAdministradores.jsp");
			}
		}
		
		
		
		
//registrar	
		if (request.getParameter("registrarUsuario")!=null) {
			response.getWriter().append("Served at: Registro").append(request.getContextPath());
			System.out.println("Entra a CRegistro");
			per= new MUsuario();	
			per.setNombreUsuario(request.getParameter("nombreUsuario"));
			per.setNombre(request.getParameter("nombre"));
			per.setCorreo(request.getParameter("correo"));
			per.setTelefono(request.getParameter("telefono"));
			per.setPassword(request.getParameter("contrasena"));
			per.setTipoUsuario(Integer.parseInt(request.getParameter("tipoUsuario")));
			
			
			
			//validamos que no exista antes de a�adir
			String res=dao.validaUsuario(per.getNombre(), per.getPassword());

			if (!res.equals("00")) {
				System.out.println("No existe el usuario se puede proceder");
				
				//llamamos a la funcion de addDB
				dao.crearUsuario(per);
				
				//Se redirecciona a pesta�a usuario o administrador
				if (request.getParameter("tipoUsuario").equals("0")) {
					response.sendRedirect("/Proyecto_Eccomers/vistas/main/main.jsp");
				}else {
					response.sendRedirect("/Proyecto_Eccomers/vistas/admin/adminAdministradores/adminAdministradores.jsp");
				}
				
			}else {
				System.out.println("Error ya existe");
				response.sendRedirect("/Proyecto_Eccomers/vistas/admin/adminUsuario/anadirUsuario.jsp");
			}
			
		}
		
//editar
		if (request.getParameter("editarUsuario")!=null) {
			System.out.println("Entra a CEditaUsuari");
			per= new MUsuario();	
			per.setNombreUsuario(request.getParameter("nombreUsuario"));
			per.setNombre(request.getParameter("nombre"));
			per.setCorreo(request.getParameter("correo"));
			per.setTelefono(request.getParameter("telefono"));
			per.setPassword(request.getParameter("contrasena"));
			per.setId(Integer.parseInt((request.getParameter("id"))));
			
			if(dao.editarUsuario(per).equals("00")){
				System.out.println("Redireccionando correcto");
				//Se redirecciona a pesta�a usuario o administrador
				System.out.println("parametro: "+request.getParameter("tipoUsuario").equals("0"));
				if (request.getParameter("tipoUsuario").equals("0")) {
					response.sendRedirect("/Proyecto_Eccomers/vistas/admin/adminUsuario/adminUsuario.jsp");
				}else {
					response.sendRedirect("/Proyecto_Eccomers/vistas/admin/adminAdministradores/adminAdministradores.jsp");
				}
			}	
		}	
		
		
		//imprimir lista de articulos
				if (request.getParameter("imprimir")!=null) {
					System.out.println("Entra a controller imprimir");
					dao.imprimir();
					response.sendRedirect("/Proyecto_Eccomers/vistas/admin/adminUsuario/adminUsuario.jsp");
				}
		
		
		//imprimir lista de articulosPDF
				if (request.getParameter("imprimirPDF")!=null) {
					System.out.println("Entra a controller imprimirPDF");
					dao.imprimirPDF();
					response.sendRedirect("/Proyecto_Eccomers/vistas/admin/adminUsuario/adminUsuario.jsp");
				}
		
		//imprimir lista de articulosEXCEL
		if (request.getParameter("imprimirEXCEL")!=null) {
			System.out.println("Entra a controller imprimirEXCEL");
			dao.imprimirEXCEL();
			response.sendRedirect("/Proyecto_Eccomers/vistas/admin/adminUsuario/adminUsuario.jsp");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
