package com.develop.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.ArticuloDAO;
import com.develop.DAO.CategoriaDAO;
import com.develop.DAO.UsuarioDAO;
import com.develop.models.MArticulo;
import com.develop.models.MCategoria;
import com.develop.models.MUsuario;


@WebServlet("/ArticuloC")
public class ArticuloC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public ArticuloC() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ArticuloC").append(request.getContextPath());
		
		
		ArticuloDAO dao = new ArticuloDAO();
		CategoriaDAO daoCategoria = new CategoriaDAO();
		MArticulo per = null;
		
		
		
//borrar
		if (request.getParameter("borrar")!=null) {
			System.out.println("entra a borrado id: "+request.getParameter("id"));
			dao.eliminaArticulo(Integer.parseInt(request.getParameter("id")));
			//redireccionamiento
			response.sendRedirect("/Proyecto_Eccomers/vistas/admin/adminArticulo/adminArticulo.jsp");
		}
		
		
		
		
//editarArticulo
		if (request.getParameter("editarArticulo")!=null) {
			System.out.println("Entra a EditarArticulo");
			per= new MArticulo();
			//recuperar datos del formulario
			per.setNombre(request.getParameter("nombre"));
			per.setDescripcion(request.getParameter("descripcion"));
			per.setPrecio(Float.parseFloat(request.getParameter("precio")));
			per.setStock(Integer.parseInt(request.getParameter("stock")));
			per.setIdArticulo(Integer.parseInt(request.getParameter("id")));
			
			//valida descuento y fecha
			if (request.getParameter("descuento")!="") {
				per.setDescuento(Integer.parseInt(request.getParameter("descuento")));
			}else {
				per.setDescuento(0);
			} 
			if (request.getParameter("fecha")!="") {
				per.setFechaExDescuento(LocalDate.parse(request.getParameter("fecha"), DateTimeFormatter.ofPattern("yyyy-MM-dd")));
			}else {
				per.setFechaExDescuento(null);
			}
			
			//obtiene id de categoria
			for (MCategoria categoria : daoCategoria.getCategorias()) {
				if(categoria.getNombre().equals(request.getParameter("categorias"))) {
					per.setIdCategoria(categoria.getIdCategoria());
				}
			}
			
			
		
			if(dao.editarArticulo(per).equals("00")){
				System.out.println("Redireccionando correcto");
				//Se redirecciona
				response.sendRedirect("/Proyecto_Eccomers/vistas/admin/adminArticulo/adminArticulo.jsp");
			}
		}
		
//crear articulo
		if (request.getParameter("crearArticulo")!=null) {
			per = new MArticulo();
			per.setNombre(request.getParameter("nombre"));
			per.setDescripcion(request.getParameter("descripcion"));
			per.setPrecio(Integer.parseInt(request.getParameter("precio")));
			per.setStock(Integer.parseInt(request.getParameter("stock")));
			
			if (request.getParameter("descuento")!="") {
				per.setDescuento(Integer.parseInt(request.getParameter("descuento")));
			}else {
				per.setDescuento(0);
			} 
			
			if (request.getParameter("fecha")!="") {
				per.setFechaExDescuento(LocalDate.parse(request.getParameter("fecha"), DateTimeFormatter.ofPattern("yyyy-MM-dd")));
			}else {
				per.setFechaExDescuento(null);
			}
			
			//meter Idcategoria
			for (MCategoria categoria : daoCategoria.getCategorias()) {
				if(categoria.getNombre().equals(request.getParameter("categorias"))) {
					per.setIdCategoria(categoria.getIdCategoria());
				}
			}
			
			
			if(dao.addArticulo(per).equals("00")){
				System.out.println("Redireccionando correcto");
				//Se redirecciona
				response.sendRedirect("/Proyecto_Eccomers/vistas/admin/adminArticulo/adminArticulo.jsp");
			}
		}
		
//imprimir lista de articulos
		if (request.getParameter("imprimir")!=null) {
			System.out.println("Entra a controller imprimir");
			dao.imprimir();
			response.sendRedirect("/Proyecto_Eccomers/vistas/admin/adminArticulo/adminArticulo.jsp");
		}
		
//imprimir lista de articulos
		if (request.getParameter("imprimirPDF")!=null) {
			System.out.println("Entra a controller imprimirPDF");
			dao.imprimirPDF();
			response.sendRedirect("/Proyecto_Eccomers/vistas/admin/adminArticulo/adminArticulo.jsp");
		}
		
//imprimir lista de articulosEXCEL
				if (request.getParameter("imprimirEXCEL")!=null) {
					System.out.println("Entra a controller imprimirEXCEL");
					dao.imprimirEXCEL();
					response.sendRedirect("/Proyecto_Eccomers/vistas/admin/adminArticulo/adminArticulo.jsp");
				}
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
