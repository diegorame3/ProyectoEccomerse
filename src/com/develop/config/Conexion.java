package com.develop.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import com.develop.controller.ArticuloC;



public class Conexion {
	public static final String URL = "jdbc:mysql://localhost:3306/db_ejemplo";
    public static final String USER = "root";
    public static final String CLAVE = "";
    Connection con;
	 	    
	    public Conexion() {
	    	 try {
	             Class.forName("com.mysql.jdbc.Driver");
	             con = (Connection) DriverManager.getConnection(URL, USER, CLAVE);
	             //System.out.println("Conexion exitosa" );
	         } catch (Exception e) {
	             System.out.println("Error en Conexion: " + e.getMessage());
	         }
		}
	    
	    public Connection getConnection() {
	    	return con;
	    }
	}


