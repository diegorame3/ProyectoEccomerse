
package com.develop.interfaces;

import com.develop.models.MArticulo;

import java.util.ArrayList;
import java.util.List;


public interface IArticulo {
    
    public ArrayList<MArticulo> getArticulo();
    public String addArticulo(MArticulo articulo);
    public String eliminaArticulo(int id);
    public ArrayList<MArticulo> getArticuloCategoria(int idCategoria);
    //public String updateArticulo();
    
}
