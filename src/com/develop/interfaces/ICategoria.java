
package com.develop.interfaces;

import com.develop.models.MCategoria;

import java.util.List;


public interface ICategoria {
    
    public List<MCategoria> getCategorias();
    public int getIdCategoria(String nombre);
    //public String addCategoria();
    //public String deleteCategoria();
    //public String updateCategoria();
    
}
