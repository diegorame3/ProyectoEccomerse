
package com.develop.interfaces;

import java.util.List;


import com.develop.models.MUsuario;

public interface IUsuario {
    
	public String validaUsuario(String nombre,String password);
	
	public String crearUsuario(MUsuario usuario);
	
	public List<MUsuario> getUsuario();
	
	public String eliminarUsuario(int id);
    
    
}
