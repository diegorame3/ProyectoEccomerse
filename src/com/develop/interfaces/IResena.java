
package com.develop.interfaces;

import com.develop.models.MResena;
import java.util.List;


public interface IResena {
    
    public List<MResena> getResena();
    public String addResena();
    public String deleteResena();
    public String updateResena();
    
}
