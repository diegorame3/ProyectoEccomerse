<%-- 
    Document   : login.jsp
    Created on : Oct 6, 2021, 5:16:12 PM
    Author     : ogeid
--%>

<%--

<%@page import="com.develop.model.MUsuario"%>
<%@page import="com.develop.config.ConexionDB"%>

--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
<html>

<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="../login/estilosLogin.css"
	media="screen" />
</head>

<body>
	<div class="sidenav">
		<div class="login-main-text">
			<h2>
				Centro comercial<br>
				<br> Ingresa y encuentra lo que estas buscando
			</h2>
			<p>
				<br>
				<br>Login or register para obtener acceso.
			</p>
		</div>
	</div>
	<div class="main">
		<div class="col-md-6 col-sm-12">
			<div class="login-form">
				<%-- <form method="post" action="../main/mainAdmin.jsp"> --%>
				<form method="POST" action="/Proyecto_Eccomers/UsuarioC">
					<div class="form-group">
						<label>Nombre de Usuario</label> <input type="text" name="usuario"
							class="form-control" placeholder="Nombre de Usuario">
					</div>
					<div class="form-group">
						<label>Contraseña</label> <input type="password" name="contrasena"
							class="form-control" placeholder="Contrasena">
					</div>
					<button type="submit" name="iniciarSecion" i class="btn btn-black">Login</button>

				</form>
				<form action="../registro/registro.jsp">
					<button type="submit" name="registro" class="btn btn-secondary">Register</button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
