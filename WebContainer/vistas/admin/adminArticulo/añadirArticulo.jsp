<%@page import="com.develop.models.MCategoria"%>
<%@page import="com.develop.DAO.CategoriaDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container">
		<h2>Creacion de Articulo</h2>
		<form method="POST" action="/Proyecto_Eccomers/ArticuloC"
			class="was-validated">
			<div class="form-group">
				<label for="uname">Nombre del Producto:</label> <input type="text"
					class="form-control" id="uname" placeholder="Ingrese nombre"
					name="nombre" required>
				<div class="valid-feedback">Valid.</div>
				<div class="invalid-feedback">Por favor ingresa el nombre.</div>
			</div>
			<div class="form-group">
				<label for="uname">Descripcion:</label> <input type="text"
					class="form-control" id="uname"
					placeholder="Ingrese la descripcion" name="descripcion" required>
				<div class="valid-feedback">Valid.</div>
				<div class="invalid-feedback">Por favor ingrese la descripcion</div>
			</div>
			<div class="form-group">
				<label for="uname">Precio unitario:</label> <input type="number"
					class="form-control" id="uname" placeholder="Ingrese precio"
					name="precio" required>
				<div class="valid-feedback">Valid.</div>
				<div class="invalid-feedback">No ha ingresado el precio.</div>
			</div>
			<div class="form-group">
				<label for="uname">Piezas disponibles:</label> <input type="number"
					class="form-control" id="uname" placeholder="Ingrese numero"
					name="stock" required>
				<div class="valid-feedback">Valid.</div>
				<div class="invalid-feedback">NO ha ingresado un numero</div>
			</div>

			<div class="form-group">
				<label>Seleccione Categoria</label> <select name="categorias">
					<optgroup label="Categorias:">
						<%--  insertar codigo para mandar a traer las categorias --%>

						<p>
							<% CategoriaDAO dao = new CategoriaDAO();
		            for (MCategoria categoria : dao.getCategorias()) {
		                   %><option><%= categoria.getNombre()%></option>
							<%}%>
						</p>
					</optgroup>
				</select>
			</div>


			<div class="form-group">
				<label for="uname">Ingrese el porcentaje de descuento a
					aplicar:</label> <input type="number" class="form-control" id="uname"
					placeholder="Ingrese el porcentaje" name="descuento">
				<div class="valid-feedback">Valid.</div>
				<div class="invalid-feedback">Si no existe agregue un '0'</div>
			</div>

			<div class="form-grup">
				<label for="uname">Fecha limite del descuento:</label> <label
					class="form-check-label"> <input class="form-check-input"
					type="date" name="fecha"> I agree on blabla. </lavel>
			</div>

			<button type="submit" name="crearArticulo" class="btn btn-primary">Submit</button>
		</form>
	</div>

</body>
</html>
