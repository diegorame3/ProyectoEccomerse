<%@page import="com.develop.models.MArticulo"%>
<%@page import="com.develop.DAO.ArticuloDAO"%>
<%@page import="com.develop.models.MUsuario"%>
<%@page import="com.develop.DAO.UsuarioDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link

	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet" />
<link rel="stylesheet" href="estilosAdminArticulo.css" type="text/css">
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<tr>
							<h5 class="card-title text-uppercase mb-0">Administrador de
								Articulos</h5>
							<tr>
							<td>
							<th>
							<form action="aņadirArticulo.jsp">
								<button type="submit" name="aa"
									class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
									<i class="fa fa-plus-circle fa-2x"></i>
								</button>
							</form>
							</th>
							<th>
							<form action=<%="/Proyecto_Eccomers/ArticuloC"%> method="POST">
								<button type="submit" name="imprimir"
									class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
									<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-file-earmark-text-fill" viewBox="0 0 16 16">
  <path d="M9.293 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.707A1 1 0 0 0 13.707 4L10 .293A1 1 0 0 0 9.293 0zM9.5 3.5v-2l3 3h-2a1 1 0 0 1-1-1zM4.5 9a.5.5 0 0 1 0-1h7a.5.5 0 0 1 0 1h-7zM4 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm.5 2.5a.5.5 0 0 1 0-1h4a.5.5 0 0 1 0 1h-4z"/>
</svg>
								</button>
							</form>
							</th>
							<th>
							<form action=<%="/Proyecto_Eccomers/ArticuloC"%> method="POST">
								<button type="submit" name="imprimirPDF"
								class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">	
									<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-file-earmark-pdf-fill" viewBox="0 0 16 16">
  <path d="M5.523 12.424c.14-.082.293-.162.459-.238a7.878 7.878 0 0 1-.45.606c-.28.337-.498.516-.635.572a.266.266 0 0 1-.035.012.282.282 0 0 1-.026-.044c-.056-.11-.054-.216.04-.36.106-.165.319-.354.647-.548zm2.455-1.647c-.119.025-.237.05-.356.078a21.148 21.148 0 0 0 .5-1.05 12.045 12.045 0 0 0 .51.858c-.217.032-.436.07-.654.114zm2.525.939a3.881 3.881 0 0 1-.435-.41c.228.005.434.022.612.054.317.057.466.147.518.209a.095.095 0 0 1 .026.064.436.436 0 0 1-.06.2.307.307 0 0 1-.094.124.107.107 0 0 1-.069.015c-.09-.003-.258-.066-.498-.256zM8.278 6.97c-.04.244-.108.524-.2.829a4.86 4.86 0 0 1-.089-.346c-.076-.353-.087-.63-.046-.822.038-.177.11-.248.196-.283a.517.517 0 0 1 .145-.04c.013.03.028.092.032.198.005.122-.007.277-.038.465z"/>
  <path fill-rule="evenodd" d="M4 0h5.293A1 1 0 0 1 10 .293L13.707 4a1 1 0 0 1 .293.707V14a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2zm5.5 1.5v2a1 1 0 0 0 1 1h2l-3-3zM4.165 13.668c.09.18.23.343.438.419.207.075.412.04.58-.03.318-.13.635-.436.926-.786.333-.401.683-.927 1.021-1.51a11.651 11.651 0 0 1 1.997-.406c.3.383.61.713.91.95.28.22.603.403.934.417a.856.856 0 0 0 .51-.138c.155-.101.27-.247.354-.416.09-.181.145-.37.138-.563a.844.844 0 0 0-.2-.518c-.226-.27-.596-.4-.96-.465a5.76 5.76 0 0 0-1.335-.05 10.954 10.954 0 0 1-.98-1.686c.25-.66.437-1.284.52-1.794.036-.218.055-.426.048-.614a1.238 1.238 0 0 0-.127-.538.7.7 0 0 0-.477-.365c-.202-.043-.41 0-.601.077-.377.15-.576.47-.651.823-.073.34-.04.736.046 1.136.088.406.238.848.43 1.295a19.697 19.697 0 0 1-1.062 2.227 7.662 7.662 0 0 0-1.482.645c-.37.22-.699.48-.897.787-.21.326-.275.714-.08 1.103z"/>
</svg>
								</button>
							</form>
							</th>
							<th>
							<form action=<%="/Proyecto_Eccomers/ArticuloC"%> method="POST">
								<button type="submit" name="imprimirEXCEL"
									class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
									<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-file-excel-fill" viewBox="0 0 16 16">
  <path d="M12 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM5.884 4.68 8 7.219l2.116-2.54a.5.5 0 1 1 .768.641L8.651 8l2.233 2.68a.5.5 0 0 1-.768.64L8 8.781l-2.116 2.54a.5.5 0 0 1-.768-.641L7.349 8 5.116 5.32a.5.5 0 1 1 .768-.64z"/>
</svg>
								</button>
							</form>
							</th>
							</td>
							</tr>
						</tr>
						
						

<div class="tooltip">display: inline;</div>
					</div>
					<div class="table-responsive">
						<table class="table no-wrap user-table mb-0">
							<thead>
								<tr>
									<th scope="col"
										class="border-0 text-uppercase font-medium pl-4">#</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Nombre</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Decripcion</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Categoria</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Stock</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Fecha
										surtido</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Descuento
										aplicable</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Fecha
										limite</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Acciones</th>
								</tr>
							</thead>
							<tbody>

								<% ArticuloDAO dao = new ArticuloDAO();
                  %>

								<%
                  for (MArticulo articulo : dao.getArticulo()) {
                	  %>

								<tr>
									<td class="pl-4">1</td>
									<td>
										<h5 class="font-medium mb-0"><%=articulo.getNombre() %></h5> <span
										class="text-muted"><%="Precio:"+articulo.getPrecio()%></span>
									</td>
									<td><span class="text-muted"><%=articulo.getDescripcion() %></span><br>

									</td>
									<td><span class="text-muted"><%=articulo.getIdCategoria() %></span><br>

									</td>
									<td><span class="text-muted"><%=articulo.getStock() %></span><br>
									</td>
									<td><span class="text-muted"><%="Fecha: "+articulo.getFechaCreacion().toString().split("T")[0] %></span><br>
										<span class="text-muted"><%="Hora: "+articulo.getFechaCreacion().toString().split("T")[1] %></span><br>
									</td>
									<td><span class="text-muted"><%="%"+articulo.getDescuento() %></span><br>
									</td>
									<td><span class="text-muted"><%="Fecha: "+articulo.getFechaExDescuento()%></span><br>

									</td>
									<td>
									<th>
										<form method="POST"
											action=<%="/Proyecto_Eccomers/ArticuloC?id="+articulo.getIdArticulo()%>>
											<button type="submit" name="borrar" onclick="/UsuarioC"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-trash "></i>
											</button>
										</form>
									</th>
									<th>
										<form method="POST"
											action=<%="editarArticulo.jsp?id="+articulo.getIdArticulo() %>>
											<button type="submit" name="editar"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-edit"></i>
											</button>
										</form>
									</th>

									</td>
								</tr>
								<%
                          }
                  %>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>