<%@page import="com.develop.models.MCategoria"%>
<%@page import="com.develop.DAO.CategoriaDAO"%>
<%@page import="com.develop.models.MArticulo"%>
<%@page import="com.develop.DAO.ArticuloDAO"%>
<%@page import="com.develop.models.MUsuario"%>
<%@page import="com.develop.DAO.UsuarioDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container">
		<h2>Edicion de articulo</h2>
		<form method="POST"
			action=<%="/Proyecto_Eccomers/ArticuloC?id="+request.getParameter("id") %>
			class="was-validated">

			<% ArticuloDAO dao = new ArticuloDAO();
                  %>

			<%
                  for (MArticulo articulo : dao.getArticulo()) {
                	  if(articulo.getIdArticulo()==Integer.parseInt(request.getParameter("id")) ){
                		  
                	 %>

			<div class="form-group">
				<label for="uname">Nombre Articulo:</label> <input type="text"
					class="form-control" id="uname" value=<%=articulo.getNombre() %>
					name="nombre" required>
				<div class="valid-feedback">Valid.</div>
				<div class="invalid-feedback">Por favor ingresa el nombre.</div>
			</div>

			<div class="form-group">
				<label for="uname">Descripcion :</label> <input type="text"
					class="form-control" id="uname" placeholder="Ingrese nombre"
					value=<%=articulo.getDescripcion() %> name="descripcion" required>
				<div class="valid-feedback">Valid.</div>
				<div class="invalid-feedback">Por favor ingresa la
					descripcion.</div>
			</div>
			<div class="form-group">
				<label for="uname">Precio:</label> <input type="number"
					class="form-control" id="uname" placeholder="Ingrese correo"
					value=<%=articulo.getPrecio() %> name="precio" required>
				<div class="valid-feedback">Valid.</div>
				<div class="invalid-feedback">Por favor ingresa el precio.</div>
			</div>
			<div class="form-group">
				<label for="uname">Stock Disponible:</label> <input type="number"
					class="form-control" id="uname" placeholder="Ingrese telefono"
					value=<%=articulo.getStock() %> name="stock" required>
				<div class="valid-feedback">Valid.</div>
				<div class="invalid-feedback">Por favor ingresa el stock.</div>
			</div>

			<%!//categorias  %>
			<div class="form-group">
				<label>Seleccione Categoria</label> <select name="categorias">
					<optgroup label="Categorias:">
						<%--  insertar codigo para mandar a traer las categorias --%>

						<p>
							<% CategoriaDAO dao2 = new CategoriaDAO();
		            for (MCategoria categoria : dao2.getCategorias()) {
		                   %><option><%= categoria.getNombre()%></option>
							<%}%>
						</p>
					</optgroup>
				</select>
			</div>

			<div class="form-group">
				<label for="uname">Descuento:</label> <input type="number"
					class="form-control" id="uname" placeholder="Ingrese contraseņa"
					value=<%=articulo.getDescuento() %> name="descuento" required>
				<div class="valid-feedback">Valid.</div>
				<div class="invalid-feedback">Por favor ingresa el descuento.</div>
			</div>

			<div class="form-grup">
				<label for="uname">Fecha limite del descuento:</label> <label
					class="form-check-label"> <input class="form-check-input"
					id="start" type="date" name="fecha"
					value=<%=articulo.getFechaExDescuento().toString() %>> I
					agree on blabla. </lavel>
			</div>
			<br>


			<% 
                	  }
                	  %>
			<%
       }
         %>

			<button type="submit" name="editarArticulo" class="btn btn-primary">Submit</button>
		</form>
	</div>

</body>
</html>
