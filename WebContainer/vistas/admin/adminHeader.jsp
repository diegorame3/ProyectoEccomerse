<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="styles.css">

    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="Icon">
                <!--Icono de usuario-->
                <span class="glyphicon-user"></span>
            </div>

            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item ">
                        <a class="nav-link" href="../adminPage/admin.jsp">Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../adminUsuario/adminUsuario.jsp">Usuario</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../adminAdministradores/adminAdministradores.jsp">Administradores</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../adminProvedores/adminProvedores.jsp">Provedores</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../adminArticulo/adminArticulo.jsp">Articulos</a>
                    </li>
                    
                </ul>
                <span class="navbar-text">
                    
                </span>
                <span class="navbar-text">
                    <form action="/Proyecto_Eccomers/vistas/login/login.jsp">
                        <button class="btn btn-primary " type="submit"  >Salir</button>
                    </form>
                </span>

            </div>
        </nav>
    </body>
</html>