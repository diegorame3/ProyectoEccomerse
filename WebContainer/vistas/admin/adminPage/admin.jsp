<%@page import="com.develop.models.MArticulo"%>
<%@page import="com.develop.models.MUsuario"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="com.develop.DAO.ArticuloDAO"%>
<%@page import="com.develop.DAO.UsuarioDAO"%>
<%@page import="com.develop.DAO.CategoriaDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@include file="../adminHeader.jsp"%>

<!DOCTYPE html>
<html>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
<link rel="stylesheet" href="estilosAdmin.css" type="text/css">
<head>
<meta charset="ISO-8859-1">

<title>Administrador</title>
</head>
<body>


	<%//retorno de totales 
		CategoriaDAO categoriaDAO = new CategoriaDAO();
		UsuarioDAO usuarioDAO = new UsuarioDAO();
		ArticuloDAO articuloDAO = new ArticuloDAO();
		
		int contUsuario=0;
		int contAdmin=0;
		int contAticulos=0;
		
		for(MUsuario usuario:usuarioDAO.getUsuario()){
			if(usuario.getTipoUsuario()==0){
				contUsuario++;
			}else{
				contAdmin++;
			}
		}
		
		for(MArticulo articulo:articuloDAO.getArticulo()){
				contAticulos++;
			}
		
	%>


	<section class="section">
		<div class="container">
			<div
				class="row md-m-25px-b m-45px-b justify-content-center text-center">
				<div class="col-lg-8">
					<h3 class="h1 m-15px-b">Tabla de control</h3>

				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-lg-4 m-15px-tb">
					<div
						class="media box-shadow-only-hover hover-top border-all-1 border-color-gray p-15px">
						<a class="overlay-link" href="../adminUsuario/adminUsuario.jsp"></a>
						<div
							class="icon-50 theme-bg white-color border-radius-50 d-inline-block">
						</div>
						<div class="p-20px-l media-body">

							<h3 class="m-5px-tb">Usuarios</h3>
							<p class="m-0px font-small">
								Activos:<%=contUsuario %></p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-4 m-15px-tb">
					<div
						class="media box-shadow-only-hover hover-top border-all-1 border-color-gray p-15px">
						<a class="overlay-link"
							href="../adminAdministradores/adminAdministradores.jsp"></a>
						<div
							class="icon-50 theme-bg white-color border-radius-50 d-inline-block">
						</div>
						<div class="p-20px-l media-body">

							<h3 class="m-5px-tb">Administradores</h3>
							<p class="m-0px font-small">
								Activos:
								<%=contAdmin %></p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-4 m-15px-tb">
					<div
						class="media box-shadow-only-hover hover-top border-all-1 border-color-gray p-15px">
						<a class="overlay-link" href="#"></a>
						<div
							class="icon-50 theme-bg white-color border-radius-50 d-inline-block">
						</div>
						<div class="p-20px-l media-body">

							<h3 class="m-5px-tb">Provedores</h3>
							<p class="m-0px font-small">Activos: 4444</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-4 m-15px-tb">
					<div
						class="media box-shadow-only-hover hover-top border-all-1 border-color-gray p-15px">
						<a class="overlay-link" href="../adminArticulo/adminArticulo.jsp	"></a>
						<div
							class="icon-50 theme-bg white-color border-radius-50 d-inline-block">
						</div>
						<div class="p-20px-l media-body">

							<h3 class="m-5px-tb">Articulos</h3>
							<p class="m-0px font-small">
								Activos:
								<%=contAticulos %></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


</body>
</html>