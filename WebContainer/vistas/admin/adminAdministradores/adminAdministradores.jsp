<%@page import="com.develop.models.MUsuario"%>
<%@page import="com.develop.DAO.UsuarioDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet" />
<link rel="stylesheet" href="estilosAdminAdministradores.css"
	type="text/css">
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<tr>
							<h5 class="card-title text-uppercase mb-0">Admministrador de Administradores</h5>
							<form action="anadirAdministrador.jsp">
								<button type="submit" name="aa"
									class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
									<i class="fa fa-user-plus"></i>
								</button>
							</form>


						</tr>
					</div>
					<div class="table-responsive">
						<table class="table no-wrap user-table mb-0">
							<thead>
								<tr>
									<th scope="col" class="border-0 text-uppercase font-medium pl-4">#</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Nombre</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Direccion</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Informacion de contacto</th>
									<th scope="col" class="border-0 text-uppercase font-medium">Creacion de la cuenta</th>
								</tr>
							</thead>
							<tbody>

					<% UsuarioDAO dao = new UsuarioDAO();%>

					<%
                  		for (MUsuario usuario : dao.getUsuario()) {
                	  		if(usuario.getTipoUsuario()==1){
                	%>

								<tr>
									<td class="pl-4">1</td>
									<td>
										<h5 class="font-medium mb-0"><%=usuario.getNombre() %></h5> <span
										class="text-muted"><%=usuario.getNombreUsuario()%></span>
									</td>
									<td><span class="text-muted">----</span><br> <span
										class="text-muted">----</span></td>
									<td><span class="text-muted"><%="Correo: "+usuario.getCorreo() %></span><br>
										<span class="text-muted"><%= "Telefono: "+usuario.getTelefono() %></span>
									</td>
									<td><span class="text-muted"><%="Fecha: "+usuario.getFecha().toString().split("T")[0] %></span><br>
										<span class="text-muted"><%="Hora: "+usuario.getFecha().toString().split("T")[1] %></span>
									</td>

									<td>
									<th>
										<form method="POST"
											action=<%="/Proyecto_Eccomers/UsuarioC?id="+usuario.getId()+"&tipoUsuario=1" %>>
											<button type="submit" name="borrar" onclick="/UsuarioC"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-trash"></i>
											</button>
										</form>
									</th>
									<th>
										<form method="POST"
											action=<%="editarAdministrador.jsp?id="+usuario.getId()%>>
											<button type="submit" name="editar"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-edit"></i>
											</button>
										</form>
									</th>


									</td>
								</tr>
								<%
                	  }
                          }
                  %>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>