<%@page import="com.develop.models.MUsuario"%>
<%@page import="com.develop.DAO.UsuarioDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../adminHeader.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container">
		<h2>Edicion de Administrador</h2>
		<form method="POST"
			action=<%="/Proyecto_Eccomers/UsuarioC?id="+request.getParameter("id")+"&tipoUsuario=1" %>
			class="was-validated">

			<% UsuarioDAO dao = new UsuarioDAO();
                  %>

			<%
                  for (MUsuario usuario : dao.getUsuario()) {
                	  if(usuario.getId()==Integer.parseInt(request.getParameter("id")) ){
                		  
                	 %>

			<div class="form-group">
				<label for="uname">Nombre Administrador:</label> <input type="text"
					class="form-control" id="uname"
					value=<%=usuario.getNombreUsuario() %> name="nombreUsuario"
					required>
				<div class="valid-feedback">Valid.</div>
				<div class="invalid-feedback">Por favor ingresa el nombre.</div>
			</div>

			<div class="form-group">
				<label for="uname">Nombre :</label> <input type="text"
					class="form-control" id="uname" placeholder="Ingrese nombre"
					value=<%=usuario.getNombre() %> name="nombre" required>
				<div class="valid-feedback">Valid.</div>
				<div class="invalid-feedback">Por favor ingresa el nombre.</div>
			</div>
			<div class="form-group">
				<label for="uname">Correo:</label> <input type="email"
					class="form-control" id="uname" placeholder="Ingrese correo"
					value=<%=usuario.getCorreo() %> name="correo" required>
				<div class="valid-feedback">Valid.</div>
				<div class="invalid-feedback">Por favor ingresa el corrreo.</div>
			</div>
			<div class="form-group">
				<label for="uname">telefono:</label> <input type="tel"
					class="form-control" id="uname" placeholder="Ingrese telefono"
					value=<%=usuario.getTelefono() %> name="telefono" required>
				<div class="valid-feedback">Valid.</div>
				<div class="invalid-feedback">Por favor ingresa el telefono.</div>
			</div>
			<div class="form-group">
				<label for="uname">Contraseņa:</label> <input type="text"
					class="form-control" id="uname" placeholder="Ingrese contraseņa"
					value=<%=usuario.getPassword() %> name="contrasena" required>
				<div class="valid-feedback">Valid.</div>
				<div class="invalid-feedback">Por favor ingresa la contrasena.</div>
			</div>


			<% 
                	  }
                	  %>
			<%
       }
         %>

			<button type="submit" name="editarUsuario" class="btn btn-primary">Submit</button>
		</form>
	</div>

</body>
</html>
