<%-- 
    Document   : login.jsp
    Created on : Oct 6, 2021, 5:16:12 PM
    Author     : ogeid
--%>

<%--

<%@page import="com.develop.model.MUsuario"%>
<%@page import="com.develop.config.ConexionDB"%>

--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
<html>

<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="../login/estilosLogin.css"
	media="screen" />
</head>

<body>
	<div class="sidenav">
		<div class="login-main-text">
			<h2>
				Centro comercial<br>
				<br> Registrate y encuentra lo que estas buscando
			</h2>
			<p>
				<br>
				<br>Ingresa tus datos para obtener acceso.
			</p>
		</div>
	</div>
	<div class="main">
		<div class="col-md-6 col-sm-12">
			<div class="login-form">
				<form method="POST" action="/Proyecto_Eccomers/UsuarioC?tipoUsuario=0">
					<div class="form-group">
						<label>Nombre de Usuario (Apodo)</label> <input type="text"
							name="nombreUsuario" class="form-control"
							placeholder="Nombre de Usuario">
					</div>
					<div class="form-group">
						<label>Nombre</label> <input type="text" name="nombre"
							class="form-control" placeholder="Nombre">
					</div>
					<div class="form-group">
						<label>Telefono</label> <input type="tel" name="telefono"
							class="form-control" placeholder="Telefono">
					</div>
					<div class="form-group">
						<label>Email</label> <input type="email" name="correo"
							class="form-control" placeholder="Ingresa tu correo">
					</div>
					<div class="form-group">
						<label>Contraseña</label> <input type="password" name="contrasena"
							class="form-control" placeholder="Contrasena">
					</div>

					<button type="submit" name="registrarUsuario"
						class="btn btn-secondary">Register</button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
