<%-- 
    Document   : mainAdmin
    Created on : Oct 6, 2021, 5:19:22 PM
    Author     : ogeid
--%>

<%@page import="com.develop.DAO.ArticuloDAO"%>
<%@page import="com.develop.models.MArticulo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.develop.models.MCategoria"%>
<%@page import="com.develop.DAO.CategoriaDAO"%>
<%@include file="header.jsp" %>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    </head>
    <body>
 
        <!-- Header-->
        <header class="bg-dark py-5">
            <div class="container px-4 px-lg-5 my-5">
                <div class="text-center text-white">
                    <h1 class="display-4 fw-bolder">Centro comercial</h1>
                    <p class="lead fw-normal text-white-50 mb-0">Todo lo que necesitas, lo encuentra aqui...</p>
                </div>
            </div>
        </header>
<!--divisor -->
<br><br><br>
<% CategoriaDAO dao = new CategoriaDAO();
	ArticuloDAO daoArticulo = new ArticuloDAO();

	for(MCategoria categoria:dao.getCategorias()){
		
		
	
%>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item active"><h3><%=categoria.getNombre()  %></h3></li>
	</ol>
	
	<!-- Solo 4 productos por categoria -->
	<section class="py-2">
		<div class="container px-4 px-lg-5 mt-5">
			<div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-left">
			
			
	<%
	System.out.println("idCat: "+categoria.getIdCategoria());
	ArrayList<MArticulo> listaArticulos = daoArticulo.getArticuloCategoria(categoria.getIdCategoria());
	int limite=(listaArticulos.size()<4)?listaArticulos.size():4;
	System.out.println("Linite: "+limite);
	
	for(int i=0;i<limite;i++){  %>
				<div class="col mb-5">
					<div class="card h-100">
						<!-- Product image-->
						<img class="card-img-top"
							src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" alt="..." />
						<!-- Product details-->
						<div class="card-body p-4">
							<div class="text-center">
								<!-- Product name-->
								<h5 class="fw-bolder"><%=listaArticulos.get(i).getNombre()  %></h5>
	<!-- Hacer cuentas para el descuento-->
								<%if(listaArticulos.get(i).getDescuento()>0){  %>
								<span class="text-muted text-decoration-line-through"><%="$"+listaArticulos.get(i).getDescuento() %></span>
								<%}  %>
								<%="$"+listaArticulos.get(i).getPrecio() %>
							</div>
						</div>
						<!-- Product actions-->
						<div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
							<div class="text-center">
								<a class="btn btn-outline-dark mt-auto" href="#">Añadir al
									carrito</a>
							</div>
						</div>
					</div>
				</div>
		
	<%
	}
	listaArticulos.clear();
	%>
	</div>
		</div>
	</section>
	<!-- Boton de ver mas...-->
	
	<%
		}
		%>
	
       
       <!-- Footer-->
       <%@include file="footer.jsp" %>
       
       <!-- Bootstrap core JS--> 
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>    



    </body>
</html>
